module gitlab.com/trantor/trantor

go 1.14

require (
	github.com/abadojack/whatlanggo v1.0.1
	github.com/alecthomas/units v0.0.0-20210208195552-ff826a37aa15 // indirect
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/go-pg/pg/v10 v10.9.1
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.2.1
	github.com/jamiealquiza/envy v1.1.0
	github.com/meskio/epubgo v0.0.0-20160213181628-90dd5d78197f
	github.com/microcosm-cc/bluemonday v1.0.9
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/prometheus/client_golang v1.10.0
	github.com/prometheus/common v0.23.0
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/spf13/cobra v1.1.3 // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.1 // indirect
	go.opentelemetry.io/otel v0.20.0 // indirect
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b
	golang.org/x/net v0.0.0-20210428140749-89ef3d95e781 // indirect
	golang.org/x/sys v0.0.0-20210426230700-d19ff857e887 // indirect
)
